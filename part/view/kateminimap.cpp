/* This file is part of the KDE libraries
   Copyright (C) 2012 Martin Sandsmark <martin.sandsmark@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "kateminimap.h"
#include "katelayoutcache.h"
#include "kateconfig.h"
#include "kateviewinternal.h"
#include "kateview.h"
#include "kateviewhelpers.h"
#include "katerenderer.h"

#include <QtGui/QPainter>
#include <QtGui/QMouseEvent>

#include <kcolorscheme.h>

KateMiniMap::KateMiniMap( KateViewInternal* internalView, QWidget *parent )
    : QWidget(parent),
      m_view(internalView->m_view),
      m_internalView(internalView)
{
    setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Minimum );
    setCursor(Qt::OpenHandCursor);
    connect(doc(), SIGNAL(textChanged(KTextEditor::Document*)), this, SLOT(updatePixmap()));
    connect(m_internalView->m_lineScroll, SIGNAL(valueChanged(int)), this, SLOT(update()));
    setMinimumWidth(200);
}

int KateMiniMap::startLine() const
{
    const uint lineHeight = m_view->renderer()->lineHeight();
    const float scrolledRatio = (float)m_internalView->m_lineScroll->value()/m_internalView->m_lineScroll->maximum();
    const int l = doc()->lines() * scrolledRatio - height()*scrolledRatio/(scaleFactor()*lineHeight);

    if (l < 0)
        return 0;
    else
        return l;

}

int KateMiniMap::endLine() const
{
    return startLine() + linesDisplayed();
}

int KateMiniMap::linesDisplayed() const
{
    return height() / scaleFactor() * m_view->renderer()->lineHeight();
}

KTextEditor::Cursor KateMiniMap::startPos() const
{
    return KTextEditor::Cursor (startLine(), 0);
}

int KateMiniMap::position() const
{
    return m_internalView->m_lineScroll->value();
    //const float scrolledRatio = (float)m_internalView->m_lineScroll->value()/m_internalView->m_lineScroll->maximum();
    //return doc()->lines() * scrolledRatio;
}


void KateMiniMap::paintEvent( QPaintEvent *event )
{
    QPainter painter(this);

    // See how far down we are
    float scrolledRatio = (float)m_internalView->m_lineScroll->value()/m_internalView->m_lineScroll->maximum();
    int offset = (m_internalView->renderer()->lineHeight() * scaleFactor() * doc()->lines()) * scrolledRatio;
    offset -= height() * scrolledRatio;
    if (offset < 0) offset = 0;

    
    // Draw the document itself
    const uint lineHeight = m_view->renderer()->lineHeight();
    int linesVisible = linesDisplayed();
    //linesVisible -= offset/(scaleFactor() * lineHeight);
    //const int lineOffset = startLine();//m_internalView->m_lineScroll->value()  - m_internalView->linesDisplayed();
    int lineOffset = m_internalView->startLine();
    //int lineOffset = offset / lineHeight;
    //lineOffset -= (((height() / scaleFactor())/m_internalView->renderer()->lineHeight()) * scrolledRatio); //offset / (scaleFactor() * m_internalView->renderer()->lineHeight());
    //lineOffset -= (linesVisible * scrolledRatio * lineHeight) / scaleFactor();
//     lineOffset /= m_internalView->renderer()->lineHeight();
    //lineOffset -= linesVisible * scrolledRatio;
    
//     int lineOffset = m_internalView->startLine();// - (height()/scaleFactor())/lineHeight;
    //if (lineOffset < 0) lineOffset = 0;
    

    const int xStart = 0;
    const int xEnd = m_pixmap.width() / scaleFactor();

    uint lineRangesSize = m_internalView->cache()->viewCacheLineCount();

    painter.save();
//     painter.translate(0, (height() - m_internalView->height() * scaleFactor())*scrolledRatio);
    painter.scale(scaleFactor(), scaleFactor());
    //paint.setRenderHints (QPainter::Antialiasing);

    for (uint line=lineOffset; line <= lineOffset + linesVisible; line++)
    {
        if ( (line >= lineRangesSize) )// || (m_internalView->cache()->viewLine(line).line() == -1) )
        {
            if (line < lineRangesSize)
                m_internalView->cache()->viewLine(line).setDirty(false);

            painter.fillRect( xStart, 0, xEnd, lineHeight, m_internalView->renderer()->config()->backgroundColor() );

        }
        else
        {
            //kDebug( 13030 )<<"KateViewInternal::paintEvent(QPaintEvent *e):cache()->viewLine"<<z;
            KateTextLayout& thisLine = m_internalView->cache()->viewLine(line);
            if (!thisLine.isValid()) {
                continue;
            }

            // If viewLine() returns non-zero, then a document line was split
            //   in several visual lines, and we're trying to paint visual line
            //   that is not the first.  In that case, this line was already
            //   painted previously, since KateRenderer::paintTextLine paints
            //   all visual lines.
            //Except if we're at the start of the region that needs to
            //be painted -- when no previous calls to paintTextLine were made.

            if (!thisLine.viewLine() || line == 0) {
                if (thisLine.viewLine())
                    painter.translate(0, lineHeight * - thisLine.viewLine());

                KTextEditor::Cursor pos = m_internalView->m_cursor;
                m_view->renderer()->paintTextLine(painter, thisLine.kateLineLayout(), xStart, xEnd, &pos);

                if (thisLine.viewLine())
                    painter.translate(0, lineHeight * thisLine.viewLine());

                thisLine.setDirty(false);
            }
        }

        painter.translate(0, lineHeight);
    }
    painter.restore();
    
    // Calculate the currently viewed rect
    const int rectHeight = m_internalView->height() * scaleFactor();
    int position = (height() - rectHeight) * scrolledRatio;
    //int position = 0;
    int rectWidth = width() * ((float)m_internalView->width() / (width()/scaleFactor()));
    if (rectWidth >= width()) rectWidth = width() - 1;
    
    
    // Shade the rest of the document
    const KColorScheme scheme(QPalette::Active, KColorScheme::View);

    QColor shadeColor = scheme.shade(KColorScheme::DarkShade);
    shadeColor.setAlpha(127);
//     painter.fillRect(0, 0, width(), position, shadeColor); // Above
//     painter.fillRect(rectWidth, position, width(), rectHeight, shadeColor); // Right of
//     painter.fillRect(0, position + rectHeight, width(), m_pixmap.height() - position - rectHeight, shadeColor); // Under
    
    painter.setRenderHint(QPainter::Antialiasing);
    // Draw a rect around the current main view
    QColor rectColor = Qt::black;
    rectColor.setAlpha(100);
    QPen rectPen(rectColor);
    rectPen.setWidth(2);
    rectColor.setAlpha(200);
    painter.setPen(rectPen);
    painter.drawRoundedRect(0, position, rectWidth, rectHeight, 5, 5);
}

void KateMiniMap::mousePressEvent(QMouseEvent* )
{
    setCursor(Qt::ClosedHandCursor);
    m_dragging = true;
}

void KateMiniMap::mouseMoveEvent( QMouseEvent *event )
{
    if (!m_dragging)
        return;

    m_internalView->m_lineScroll->setValue(event->y()*m_internalView->m_lineScroll->maximum() / qMin(height(), (int)(m_internalView->renderer()->lineHeight() * scaleFactor() * doc()->lines())));
    //updatePosition(event->y() - boxHeight()/2);
}

void KateMiniMap::mouseReleaseEvent(QMouseEvent* )
{
    setCursor(Qt::OpenHandCursor);
    m_dragging = false;
}

inline int KateMiniMap::boxHeight()
{
    return m_internalView->height() * scaleFactor();
}

inline float KateMiniMap::scaleFactor() const
{
    return 0.2f;
}

void KateMiniMap::wheelEvent(QWheelEvent* e)
{
    m_internalView->m_lineScroll->wheelEvent(e);
}

KateDocument* KateMiniMap::doc() const
{
    return m_internalView->doc();
}
void KateMiniMap::updatePixmap()
{
    update();
}
