/* This file is part of the KDE libraries
   Copyright (C) 2012 Martin Sandsmark <martin.sandsmark@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef _KATEMINIMAP_H_
#define _KATEMINIMAP_H_

#include <QtGui/QWidget>
#include <QtGui/QPixmap>

#include "katetextcursor.h"

class KateViewInternal;
class KateView;
class KateDocument;


class KateMiniMap : public QWidget
{
    Q_OBJECT
public:
    KateMiniMap( KateViewInternal* internalView, QWidget *parent );
    
    int width() const { return 200; }

public Q_SLOTS:
    void updatePixmap();
    int startLine() const;
    int endLine() const;
    int linesDisplayed() const;
    KTextEditor::Cursor startPos() const;
    int position() const;

Q_SIGNALS:
    void positionChanged( int pos );
    
private:
    void paintEvent( QPaintEvent* );
    inline int boxHeight();
    inline float scaleFactor() const;
    inline KateDocument *doc() const;
    
    // Event handling
    virtual void mousePressEvent(QMouseEvent*);
    virtual void mouseReleaseEvent(QMouseEvent*);
    virtual void mouseMoveEvent(QMouseEvent*);
    virtual void wheelEvent(QWheelEvent*);
    
    bool m_dragging;
    KateView *m_view;
    KateViewInternal *m_internalView;
    QPixmap m_pixmap;
};

#endif//KATEMINIMAP_H